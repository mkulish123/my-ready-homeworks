// 1. Які існують типи даних у Javascript?
//number, null, undefined, string, bigint, boolean, symbol, object

// 2. У чому різниця між == і ===?
// Це оператори порівняння, === це строге порівняння, 
// "==" окрім порівняння ще може використовуватись для неявного приведення типів

// 3. Що таке оператор?
// Це частина мови програмування, за допомоги якої можна задати певну дію



/* Завдання
Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем
за допомогою модальних вікон браузера - alert, prompt, confirm.

Технічні вимоги:

Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you
want to continue? і кнопками Ok, Cancel.
Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача.
Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я, або при введенні віку
вказав не число - запитати ім'я та вік наново (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).
*/


let userName = prompt('What is your name?');
let userAge = +prompt('What is your age?');

while (!userName||userName == 'null'||userName == !userName) {
    userName = prompt('Enter your name please, ', `${userName}`);
};
while (!userAge|| userAge < 1||userAge > 99 ||userAge == 'null' ) {
userAge = prompt('Enter your age please, ', `${userAge}`)
};


if (userAge > 22) {
    alert(`Welcome, ${userName}!`);  
} else if (userAge >= 18) {
    let askUser = confirm ('Are you sure you want to continue?');
    if (askUser) {
        alert(`Welcome, ${userName}!`);
    } else {
        alert ('You are not allowed to visit this website.');
    };
} else {
    alert ('You are not allowed to visit this website.');
};
